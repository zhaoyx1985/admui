var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var csso = require('gulp-csso');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');



gulp.task('less', function () {
    return gulp.src(["src/less/AdmUI.less"])
        .pipe(less())
        .pipe(gulp.dest('./dist/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(csso())
        .pipe(gulp.dest('dist/css'));
});


gulp.task('less:watch', function () {
    gulp.watch('./**/*.less', ['less,lessApp']);
});


gulp.task('angularConcat', function () {
    gulp.src([
            'vendors/bower_components/angular/angular.min.js',
            'vendors/bower_components/angular-animate/angular-animate.min.js',
            'vendors/bower_components/angular-aria/angular-aria.min.js',
            'vendors/bower_components/angular-route/angular-route.min.js',
            'vendors/bower_components/angular-messages/angular-messages.min.js',
            'vendors/bower_components/angular-material/angular-material.min.js',
            'vendors/bower_components/oclazyload/dist/ocLazyLoad.min.js',
            'vendors/bower_components/angular-loading-bar/build/loading-bar.min.js',
            'vendors/angular-bootstrap/ui-bootstrap-custom-1.1.2.min.js',
            'vendors/angular-bootstrap/ui-bootstrap-custom-tpls-1.1.2.min.js'
        ])
        .pipe(concat('angular.js'))
        .pipe(gulp.dest('dist/js'));
});


gulp.task('libsConcat', function () {
    gulp.src([
            'vendors/bower_components/jquery/dist/jquery.min.js',
            'vendors/bower_components/bootstrap/dist/js/bootstrap.min.js',
            'vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
            'vendors/bower_components/JavaScript-MD5/js/md5.min.js',
            'vendors/bower_components/jasny-bootstrap/js/fileinput.js',
            'vendors/bower_components/moment/min/moment.min.js',
        ])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('dist/js'));
});


gulp.task('libsCssConcat', function () {
    gulp.src([
            'vendors/bower_components/angular-loading-bar/build/loading-bar.min.css',
            'vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css',
            'vendors/bower_components/angular-material/angular-material.min.css',
            'vendors/bower_components/animate.css/animate.css',
            'vendors/bower_components/hint.css/hint.min.css',
            'vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css'
        ])
        .pipe(concat('libs.css'))
        .pipe(gulp.dest('dist/css'));
});



gulp.task('default', ['less',  'libsCssConcat', 'angularConcat', 'libsConcat']);
